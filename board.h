#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
#include <vector>
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    //bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    std::vector <Move*> Possible_Moves(Board* b, Side side);
    int fullHeuristic(Board *b, Move *move, Side curr_side, Side opp_side);
    int testHeuristic(Board *b, Move *move, Side curr_side, Side opp_side);
    int Row_count(Board* board, Move* m, Side side);
    int Col_count(Board* board, Move* m, Side side);
    int Score(int x, int y);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    void setBoard(char data[]);
    
    
    bool get(Side side, int x, int y);
};

#endif
