#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <cmath>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    std::vector<Move> possible_moves;
    Move *doMove(Move *opponentsMove, int msLeft);
    int Minimax(Side side, Board * b, int depth, int maxDepth, Move * curr_move, int cscore, Move *move);
    int alphabeta(Board * board, Move* move, int depth, int a, int b, Side side);
    //Move *Minimax(Side side, Board * b, int depth, int maxDepth, Move * cmove, int cscore);
    // Flag to tell if the player is running within the test_minimax context
    Side curr_side;
    Side opp_side;
    int max_score, min_x, min_y;
    bool testingMinimax;
    Board * b;
    Board * b_new;
};

#endif
