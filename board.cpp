#include "board.h"
#include <iostream>

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

std::vector<Move*> Board::Possible_Moves(Board* b, Side side)
{
	std::vector <Move*> possible_moves;
	Move* m;
	for (int r = 0; r < 8; r++)
	{
		for (int c = 0;  c < 8; c++)
		{
			m = new Move(r,c);
			if (b->checkMove(m, side) == true)
			{
				possible_moves.push_back(m);
			}
		}
	}
	return possible_moves;
}
int Board::Score(int x, int y) {
	int score;
	if ((x == 0 || x == 7) && (y ==0 || y == 7))
	{
		if (y == 1 || y == 6 || x == 1 || x == 6)
		{
			score = -30;
		}
		else if ((x == 0 && y == 7) || (x==7 && y == 0) || (x ==0 && y==0) ||(x == 7 && y==7))
		{
			score = 50;
		}
		else 
		{
			score = 25;
		}
	}
	else if ((y == 1 || y == 6) && (x ==1 || x == 6))
	{
		score = -20;
	}
	else if ((y == 2 || y == 5) && (x == 2 || x == 5))
	{
		score = 4;
	}
	else
	{
		score = 2;
	}
	return score;
	
}
int Board::Row_count(Board* board, Move* m, Side side)
{
	Board* b_row = board->copy();
	b_row->doMove(m, side);
	int count = 0;
	int i = m->getY();
	for (int j = 0; j < 8; j ++)
	{
		if (b_row->get(side, i, j))
		{
			count += 1;
		}
	}
	if (count >= 6)
	{
		if(i == 0 || i == 7)
		{
			return count*3;
		}
		else
		{
			return count * 2;
		}
	}
	else
	{
		return 0;
	}
}
int Board::Col_count(Board* board, Move* m, Side side)
{
	Board* b_col = board->copy();
	b_col->doMove(m, side);
	int count = 0;
	int i = m->getX();
	for (int j = 0; j < 8; j++)
	{
		if (b_col->get(side, j, i))
		{
			count += 1;
		}
	}
	if (count >= 6)
	{
		if(i == 0 || i == 7)
		{
			return count*3;
		}
		else
		{
			return count * 2;
		}
	}
	else
	{
		return 0;
	}
}
//Move* opening_book(Board *b, 
int Board::fullHeuristic(Board *b, Move *move, Side curr_side, Side opp_side)
{
	bool mobility = false;
	int max_mobility;
	int x = move->getX();
	int y = move->getY();
	int weights;
	if (b->count(opp_side)/b->count(curr_side) >= 2) 
	 {
		 mobility = true;
		Board* b_try = b->copy();
		b_try->doMove(move, curr_side);
		int move_diff = b_try->Possible_Moves(b_try,curr_side).size() - (b_try->Possible_Moves(b_try,opp_side)).size();
		max_mobility = Score(x,y) * move_diff;
		if (max_mobility > 0)
		{
			return max_mobility;
		}
		else
		{
			mobility = false;
		}
	}
	
	 if (mobility == false)
	 {
		Board* b_new = b->copy();
		b_new->doMove(move, curr_side);		
		int top = (b_new->count(curr_side) - b_new->count(opp_side)) - (b->count(curr_side) - b->count(opp_side));
		weights = Score(x,y) * top;
		return weights;
	}
}

int Board::testHeuristic(Board *b, Move *move, Side curr_side, Side opp_side)
{
	Board *b_test = b->copy();
	b_test->doMove(move, curr_side);
	return b_test->count(curr_side) - b_test->count(opp_side);
}
/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
