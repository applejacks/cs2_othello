#include "player.h"
#include <cstdio>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = true;
    curr_side = side;   
    if (curr_side == BLACK)
     {		 
		opp_side = WHITE;
	}
	else 
	{
		opp_side = BLACK;
	}
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

     b = new Board();
     //create board
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     std::vector <Move*> moves;
     bool mobility = false;
     b->doMove(opponentsMove, opp_side);
	 if (b->count(opp_side)/b->count(curr_side) >= 2)
	 {
		 mobility = true;
		 int max_mobility;
		 int max_score = 0;
		 int max_x, max_y;
		 moves = b->Possible_Moves(b, curr_side);
		 for (int i = 0; i < (int)moves.size(); i++)
		 {
			 	int x = moves[i]->getX();
				int y = moves[i]->getY();
				Board* b_try = b->copy();
				b_try->doMove(moves[i], curr_side);
				max_mobility = ((b_try->Possible_Moves(b_try,curr_side)).size()) - ((b_try->Possible_Moves(b_try,opp_side)).size());
				if (max_mobility > max_score)
				{
					max_score = max_mobility;
					max_x = x;
					max_y = y;
				}
		}		
		if (max_score > 0)
		{				
			Move best_move = Move(max_x, max_y);
			b->doMove(&best_move, curr_side);
			Move * my_move = new Move(max_x, max_y);
			return my_move;
		}
		else
		{
			mobility = false;
		}
	}
	
	 if (b->count(opp_side)/b->count(curr_side) < 2 || mobility == false)
	 {
		max_score = -1280;
		moves = b->Possible_Moves(b, curr_side);
		for (int i = 0; i < (int)moves.size(); i++)
		{
			int x = moves[i]->getX();
			int y = moves[i]->getY();
			b_new = b->copy();
			b_new->doMove(moves[i], curr_side);
			int score_i = b->Score(x,y) + b->Row_count(b, moves[i], curr_side) + b->Col_count(b, moves[i], curr_side);
			int score_val = score_i * ((b_new->count(curr_side) - b_new->count(opp_side)) - (b->count(curr_side) - b->count(opp_side)));
			if (score_val > max_score)
			{
				max_score = score_val;
				min_x = x;
				min_y = y;
			}
		}

		if (max_score != -1280)
		{
			Move best_move = Move(min_x, min_y);
			b->doMove(&best_move, curr_side);
			Move * my_move = new Move(best_move.getX(),best_move.getY());
			return my_move;
		}
		else
		{
			return NULL;
		}
	
	}
}
